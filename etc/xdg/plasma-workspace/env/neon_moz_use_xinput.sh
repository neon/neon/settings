# SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2020 Harald Sitter <sitter@kde.org>

# On X11, with MOZ_USE_XINPUT2=1 set in the environment Firefox exhibits much
# improved pixel-by-pixel touchpad scrolling, and touchscreen scrolling
# actually works. On Wayland, the behavior is already enabled by default so
# the environment variable has no effect.
# This requires kwin>=5.19.3 to work properly.
# https://phabricator.kde.org/T13335

export MOZ_USE_XINPUT2=1
