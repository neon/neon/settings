# SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2021 Harald Sitter <sitter@kde.org>

# Explicitly enable workers forking from the app using KIO rather than centralized through klauncher.
# This is neon unstable only! The motivation is to get a bit broader testing which we can afford on
# unstable, what with being focused on KDE development anyway.
# https://phabricator.kde.org/T12140

export KDE_FORK_SLAVES=1
