# frozen_string_literal: true

# SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2021 Harald Sitter <sitter@kde.org>

require_relative 'test_helper'
require_relative '../usr/lib/neon_update/kernel_auto'

class KernelAutoTest < Minitest::Test
  def test_run
    status = mock('ProcessStatus')
    status.stubs(:success?).returns(true)
    Open3
      .expects(:capture2)
      .with(*%w[apt-mark showmanual])
      .returns([File.read("#{__dir__}/data/kernel_auto.apt-mark"), status])

    marker = ManualVersionedKernels.new
    assert_equal(['linux-cloud-tools-5.11.0-22-generic',
                  'linux-headers-5.11.0-22-generic',
                  'linux-headers-5.11.0-25-generic',
                  'linux-headers-5.8.0-53-generic',
                  'linux-headers-5.8.0-55-generic',
                  'linux-headers-5.8.0-59-generic',
                  'linux-headers-5.8.0-63-generic',
                  'linux-hwe-5.11-cloud-tools-5.11.0-22',
                  'linux-hwe-5.11-headers-5.11.0-22',
                  'linux-hwe-5.11-headers-5.11.0-25',
                  'linux-hwe-5.11-tools-5.11.0-22',
                  'linux-hwe-5.11-tools-5.11.0-25',
                  'linux-hwe-5.8-headers-5.8.0-53',
                  'linux-hwe-5.8-headers-5.8.0-55',
                  'linux-hwe-5.8-headers-5.8.0-59',
                  'linux-hwe-5.8-headers-5.8.0-63',
                  'linux-hwe-5.8-tools-5.8.0-55',
                  'linux-image-5.11.0-22-generic',
                  'linux-image-5.11.0-25-generic',
                  'linux-image-5.3.0-46-generic',
                  'linux-image-5.8.0-53-generic',
                  'linux-image-5.8.0-55-generic',
                  'linux-image-5.8.0-59-generic',
                  'linux-image-5.8.0-63-generic',
                  'linux-modules-5.11.0-22-generic',
                  'linux-modules-5.11.0-25-generic',
                  'linux-modules-5.3.0-46-generic',
                  'linux-modules-5.8.0-53-generic',
                  'linux-modules-5.8.0-55-generic',
                  'linux-modules-5.8.0-59-generic',
                  'linux-modules-5.8.0-63-generic',
                  'linux-modules-extra-5.11.0-22-generic',
                  'linux-modules-extra-5.11.0-25-generic',
                  'linux-modules-extra-5.8.0-53-generic',
                  'linux-modules-extra-5.8.0-55-generic',
                  'linux-modules-extra-5.8.0-59-generic',
                  'linux-modules-extra-5.8.0-63-generic',
                  'linux-tools-5.11.0-22-generic',
                  'linux-tools-5.11.0-25-generic',
                  'linux-tools-5.8.0-55-generic'].sort,
                 marker.list.sort)
  end

  def test_fail # raise if apt-mark fails
    status = mock('ProcessStatus')
    status.stubs(:success?).returns(false)
    Open3
      .expects(:capture2)
      .with(*%w[apt-mark showmanual])
      .returns(['', status])
    assert_raises { ManualVersionedKernels.new.list.sort }
  end
end
