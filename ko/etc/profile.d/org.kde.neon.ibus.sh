# SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2020 Jonathan Riddell <jr@jriddell.org>
# SPDX-FileCopyrightText: 2021 Harald Sitter <sitter@kde.org>

# Only setup ibus input method outside wayland. On wayland ibus should talk to
# the compositor through text input protocols to kick into action and explicitly
# setting an IM gets in the way of the virtual keyboard integration.
# https://bugs.kde.org/show_bug.cgi?id=439469
if [ "$XDG_SESSION_TYPE" = "x11" ]; then
  export QT_IM_MODULE=ibus
  export GTK_IM_MODULE=ibus
fi
export XMODIFIERS=@im=ibus
